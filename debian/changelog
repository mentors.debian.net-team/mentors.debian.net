mentors.debian.net (15) stable; urgency=medium

  * Add Dependency on python3-distro-info.
  * Set XB-Popcon-Reports: no
  * Bump Standards-Version to 4.6.2.

 -- Mattia Rizzolo <mattia@debian.org>  Thu, 13 Jul 2023 15:54:57 +0200

mentors.debian.net (14) stable; urgency=medium

  * Bump debhelper compat level.
  * Bump Standards-Version to 4.6.0.
  * Drop alternative to python-celery-common \o/

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 10 Oct 2021 15:54:28 +0200

mentors.debian.net (13) stable; urgency=medium

  * Add dependencies for django rest api
  * Fix Standard-version for buster

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Tue, 11 Aug 2020 11:37:45 +0200

mentors.debian.net (12) stable; urgency=medium

  * Make tag guideline work with gbp tag
  * Remove pylons and python2 dependencies
  * Add libapache2-mod-wsgi-py3 dependency (replaces python2 version of
    apache mod_wsgi)
  * Sort dependencies

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Mon, 20 Jul 2020 19:03:00 +0200

mentors.debian.net (11) stable; urgency=medium

  [ Mattia Rizzolo ]
  * Dependency changes:
    * Alternate dependency on python-celery common with "celery (>= 4.4.2-3)"
      as it's in bullseye.

  [ Baptiste BEAUPLAT ]
  * Add dependency python3-dulwich

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Tue, 09 Jun 2020 21:44:45 +0200

mentors.debian.net (10) stable; urgency=medium

  * Add new dependency: python3-lxml

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Mon, 04 May 2020 16:00:17 +0200

mentors.debian.net (9) stable; urgency=medium

  * Dependency changes:
    + python3-debianbts
      (new in c6243caf872bb5f8a557f0460052c65af268b42c)
  * Bump Standards-Version to 4.5.0, no changes needed.
  * Update copyright.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 27 Apr 2020 18:13:40 +0200

mentors.debian.net (8) stable; urgency=medium

  * Update dependencies to use django-2 (available from buster-backports)

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Sat, 28 Mar 2020 17:57:47 +0100

mentors.debian.net (7) stable; urgency=medium

  * Add missing dependency for installing celery binary

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Thu, 30 Jan 2020 15:56:28 +0100

mentors.debian.net (6) stable; urgency=medium

  * Add dependency for redis and celery
  * Add dependency for mocking redis
  * Add missing dependency for python3 debian module

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Thu, 30 Jan 2020 13:02:32 +0100

mentors.debian.net (5) stable; urgency=medium

  [ Mattia Rizzolo ]
  * Dependency changes:
    + alembic (for the CLI tool)

  [ Baptiste BEAUPLAT ]
  * Bump std-version to 4.4.1: no changes needed.
  * Add myself to the uploaders.
  * Dependency changes:
    + ca-certificates
    + python3, python3-bcrypt, python3-django
      (needed for the new django-based website)

 -- Mattia Rizzolo <mattia@debian.org>  Thu, 31 Oct 2019 16:00:47 +0100

mentors.debian.net (4) stable; urgency=medium

  * Dependency changes:
    + python-alembic
    (new in b5cce5b8d9a13959478e025664fc1597490ead54)

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 17 Feb 2019 00:37:23 +0100

mentors.debian.net (3) stable; urgency=medium

  * Dependency changes:
    - python-soappy
      (not used anymore since 0e4ce3a6a67f4423d97b968c4404752dc127717c)
    + python-passlib
    + python-bcrypt
      (both new in 63d45315cb298baa04113ccf5d5363e3a067d7bb)

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 16 Feb 2019 19:38:49 +0100

mentors.debian.net (2) stable; urgency=medium

  * Move maintenance of this package to git.
  * Update dependencies.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 03 Feb 2019 00:30:20 +0100

mentors.debian.net (1.0) unstable; urgency=low

  * First version

 -- root <root@wv-debian-mentors1.wavecloud.de>  Sat, 06 Oct 2012 23:23:13 +0000
